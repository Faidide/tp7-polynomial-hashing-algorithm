#include "hash.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

// precomputed variables for the mod339 function
#define MOD339_N 8589934583uLL
// smallest k such as 2^k > n
#define MOD339_K 33uLL
// floor of 4^k / n
#define MOD339_R 8589934601uLL
// 4^k
#define MOD339_4K 73786976294838206464.0l

#define MOD339_M 1uLL

// masks to split a and b un mul339
// (1<<17)-1
#define LOWER_BIT_MASK 131071uLL

#define TWO_POW_33_MOD 9uLL
#define TWO_POW_17_MOD 131072uLL
#define TWO_POW_34_MOD 18uLL
#define TWO_POW_66_MOD 81uLL

// used by multiplication
uint64_t a0, a1, b0, b1, a1b1, a1b0, a0b1, a0b0;

/* Run a multiplication module 2^33 - 9 */
uint64_t mul339 (uint64_t a, uint64_t b) {
  // for integers between 0 and (2^33)-9

  // split a and b in a0 a1 and b0 b1
  a0 = LOWER_BIT_MASK & a;
  a1 = a >> 17;
  b0 = LOWER_BIT_MASK & b;
  b1 = b >> 17;
  // TODO: cast to uint64_t to prevent eventual implicit overflow
  // compute mod(a1*b1)
  a1b1 = mod339 (a1 * b1);
  // compute mod(a1*b0)
  a1b0 = mod339 (a1 * b0);
  // compute mod(a1*b0)
  a0b1 = mod339 (a0 * b1);
  // compute mod(a0*b0)
  a0b0 = mod339 (a0 * b0);

  // mod(a1*b1)*mod(2^2r) + mod(a1*b0 + a0*b1)*mod(2^r) + mod(a0*b0)
  return mod339 (a1b1 * TWO_POW_34_MOD + (a1b0 + a0b1) * TWO_POW_17_MOD + a0b0);
}

uint64_t mod339_temp, mod339_temp2;
/* compute modulo using barrett reduction algorithm */
/* https://www.nayuki.io/page/barrett-reduction-algorithm */
/* https://www.wikiwand.com/en/Barrett_reduction */
/* https://en.wikipedia.org/wiki/Barrett_reduction */
uint64_t mod339 (uint64_t t) {
  mod339_temp = t >> MOD339_K;
  mod339_temp2 = t - mod339_temp * MOD339_N;
  if (MOD339_N <= mod339_temp2)
    mod339_temp2 -= MOD339_N;
  return mod339_temp2;
}

/* Hash the integer k using polynomial hash modulo 2^33 - 9 */
uint64_t hash339 (uint64_t k, void *buf, size_t buflen) {
  // value that will hold the result
  uint64_t result = 0;
  // how many full blocks of size len(uint64_t) do we have here ?
  int nb_blocks =
      (int)(floorl ((long double)buflen / (long double)sizeof (uint64_t)));
  // what is the size of the remaining ?
  int remaining_size = buflen % sizeof (uint64_t);
  // set the array to work with
  uint64_t *blocks = buf;

  // initiate result to first item (horner eval)
  result = mul339 (blocks[nb_blocks - 1], k);
  // loop to iterate through blocks and compute polynomial
  for (int i = nb_blocks - 2; i > 0; i++) {
    result = mod339 (mul339 (result, k) + blocks[i]);
  }

  // create value to hold remaining buffer
  uint64_t remaining_buf = 0;
  char * byte_pointer;
  // for each byte, copy it  
  for(int i= 0; i < remaining_size; i++) {
    byte_pointer = buf + (sizeof(uint64_t)*nb_blocks) + i;
    remaining_buf = remaining_buf + (uint64_t)((*byte_pointer)<<((remaining_size-i-1)*8));
  }

  // add remaining
  result = mod339 (mul339 (result, k) + remaining_buf);
  // if we don't do that we will start at i=0 and very small variation of hash may occur
  result = mul339(result,k);

  // return the result
  return result;
}