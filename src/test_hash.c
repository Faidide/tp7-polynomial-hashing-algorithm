#include "hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

int main (int argc, char *argv[]) {

  printf ("Lancement du programme de test pour hash\n");

  char testchar[10] = "niquhhyku";
  char testchar2[10] = "niquhhyfu";
  char testchar3[10] = "ngquhhlfu";
  char testchar4[10] = "niquhhyku";
  char testchar5[10] = "niquhhykv";
  // hash this one
  printf("hash de la chaine 1 :%" PRId64 "\n", hash339((uint64_t)10, testchar, 10) );
  printf("hash de la chaine 2 :%" PRId64 "\n", hash339((uint64_t)10, testchar2, 10) );
  printf("hash de la chaine 3 :%" PRId64 "\n", hash339((uint64_t)10, testchar3, 10) );
  printf("hash de la chaine 4 :%" PRId64 "\n", hash339((uint64_t)10, testchar4, 10) );
  printf("hash de la chaine 5 :%" PRId64 "\n", hash339((uint64_t)10, testchar5, 10) );

  return 0;
}
