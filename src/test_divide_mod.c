#include "hash.h"
#include <stdio.h>

#define TEST_DATA_LENGTH 200

int main (int argc, char *argv[]) {

  printf ("Lancement du programme de test pour la division modulo\n");

  // reading file with test data and results
  FILE *test_csv = fopen ("res/mult_mod.csv", "r");

  // test for errors
  if (test_csv == NULL) {
    perror ("fopen");
    exit (1);
  }

  uint64_t test_mod_num = 8037613846703512380;
  uint64_t res_mod = mod339(test_mod_num);

  if(res_mod!=5712192431) {
    printf("Modulo did not pass the test, expected: %" PRId64 " , got: %" PRId64"\n", 5712192431, res_mod);
  }

  // strings to store the columns names
  char col1[20];
  char col2[20];
  char col3[20];

  // read the csv header
  fscanf (test_csv, "%s,%s,%s", col1, col2, col3);

  // variables to store test line data
  uint64_t mul1, mul2, res, compt_res;

  // read each line
  for (int i = 0; i < TEST_DATA_LENGTH; i++) {
    // get current line data
    fscanf (test_csv, "%" PRId64 ",%" PRId64 ",%" PRId64 "\n", &mul1, &mul2, &res);
    // compute the results and compare
    compt_res = mul339 (mul1, mul2);
    // test for valid result against csv data
    if (res != compt_res) {
      printf ("Test failed, expected %" PRId64 ", got %" PRId64
              " for multiplication between %" PRId64 " and %" PRId64 " \n",
              res, compt_res, mul1, mul2);
      exit (1);
    }
  }

  // testing the results of 2^32 * 2^32
  compt_res = mul339 (4294967296, 4294967296);
  // validating the result
  if(compt_res != 2147483666) {
    printf ("Test failed, expected %" PRId64 ", got %" PRId64
            " for multiplication between %" PRId64 " and %" PRId64 " \n",
            2147483666, compt_res, 4294967296, 4294967296);
    exit (1);
  }

  // testing the results of 2^32 * 2^32
  compt_res = mul339 (4294967297, 4294967297);
  // validating the result
  if(compt_res != 2147483676) {
    printf ("Test failed, expected %" PRId64 ", got %" PRId64
            " for multiplication between %" PRId64 " and %" PRId64 " \n",
            2147483676, compt_res, 4294967297, 4294967297);
    exit (1);
  }

  // if nothing bad hapenned test were successfull
  printf ("All test passed.\n");

  return 0;
}