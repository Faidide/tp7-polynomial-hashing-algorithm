#include "hash_table_fun.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

int main (int argc, char *argv[]) {

  printf ("Lancement du programme de test pour hash_table\n");

  uint64_t max_uint64 = 198892;

  uint32_t res_buffer;

  struct hashing_table *ht = setup_ht (198892, 256);

  long int r1, r2;
  uint64_t rand_buf = 0;

  printf("Maximum nomber of possible values: %" PRIu64 ".\n", max_uint64);


  // for each possible value
  for(uint64_t i=0; i<=max_uint64;i++) {
      if(i%65536==0) {
          printf("step i=%" PRId64 "\n", i);
      }
      // generate random number
      r1 = random() & 0xFFFFFFFF;
      r2 = random() & 0xFFFFFFFF;
      rand_buf = (r1) + (r2<<32);
      // allocate space to store i
      char * key = malloc(sizeof(char*)*8);
      memcpy(key, &rand_buf, 8);
      // on ajoute le resultat au hash
      int nb_ajout = insert_elem_ht (ht, NULL, 0, key, 8);
  }

  // for each bucket, loop for values with similar input
  for(uint64_t i=0; i<198892;i++) {
    // for each elem in bucket
    for(int j=0; j<ht->table[i]->count;j++) {
      for(int k=0;k<ht->table[i]->count;k++) {
        if(k!=j) {
          if(test_fun(*((uint64_t*)ht->table[i]->elems[j]->key))==test_fun(*((uint64_t*)ht->table[i]->elems[k]->key))) {
            printf("Collision dans le bucket %" PRIu64 " pour les entrées %"PRIu64 " et %" PRIu64 ".\n", i, (*((uint64_t*)ht->table[i]->elems[j]->key)), (*((uint64_t*)ht->table[i]->elems[k]->key)));
          }
        }
      }
    }
  }

  // free variables
  free_ht (ht);

  printf ("Tests passés avec succès.\n");

  return 0;
}
