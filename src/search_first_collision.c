#include "hash_table_fun_first.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

int main (int argc, char *argv[]) {

  // set the random seed
  srandom(time(NULL));

  printf ("Lancement du programme de test pour hash_table\n");

  uint64_t max_uint64 = 198892;

  uint32_t res_buffer;

  struct hashing_table *ht = setup_ht (198892, 256);

  long int r1, r2;
  uint64_t rand_buf = 0;


  // for each possible value
  for(uint64_t i=0; i<=max_uint64;i++) {
      if(i%65536==0) {
          printf("step i=%" PRIu64 "\n", i);
      }
      // generate random number
      r1 = random() & 0xFFFFFFFF;
      r2 = random() & 0xFFFFFFFF;
      rand_buf = (r1) + (r2<<32);
      // allocate space to store i
      char * key = malloc(sizeof(char*)*8);
      memcpy(key, &rand_buf, 8);
      // on ajoute le resultat au hash
      int nb_ajout = insert_elem_ht (ht, NULL, 0, key, 8);
      if(nb_ajout==0) {
        printf("A collision was found at iteration %" PRIu64 " for %" PRIu64 ".\n", i, rand_buf);
        exit(0);
      }
  }

  // free variables
  free_ht (ht);

  printf ("Tests passés avec succès.\n");

  return 0;
}
