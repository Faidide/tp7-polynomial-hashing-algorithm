#include "hash_table.h"
#include "hash.h"

#include <stdio.h>
#include <string.h>

// initialize hashing table
struct hashing_table *setup_ht (size_t tablelen, size_t base_bucketlen) {

  // allocate memory
  struct hashing_table * alloc_ht = malloc (sizeof (struct hashing_table));

  // defaut variables
  alloc_ht->tablelen = tablelen;
  alloc_ht->base_bucketlen = base_bucketlen;
  // allocate table of pointers to buckets
  alloc_ht->table = malloc (tablelen * sizeof (void *));

  // allocation error management
  if (alloc_ht->table == NULL) {
    perror ("malloc");
    exit (1);
  }

  // for each buclet, allocate something
  for (int i = 0; i < tablelen; i++) {
    alloc_ht->table[i] = malloc (sizeof (struct bucket));
    // allocation error management
    if (alloc_ht->table[i] == NULL) {
      perror ("malloc");
      exit (1);
    }
    // allocated length
    alloc_ht->table[i]->len = 0;
    // length of part with values
    alloc_ht->table[i]->count = 0;
  }

  alloc_ht->k = DEFAULT_HASH_K;

  // return pointer to newly allocated object
  return alloc_ht;
}

// free memory without freeing elements inside
void free_bucket (struct bucket *b) {
  // free the structures containing the elements
  for (int i = 0; i < b->len; i++) {
    free (b->elems[i]);
  }
  // free table of elems
  free (b->elems);
  // free the bucket structure itself
  free (b);
  return;
}

// free memory and all elements
void free_bucket_full (struct bucket *b) {
  // free the elements
  for (int i = 0; i < b->len; i++) {
    free (b->elems[i]->key);
    free (b->elems[i]->data);
    free (b->elems[i]);
  }
  // free table of elems
  free (b->elems);
  // free the bucket structure itself
  free (b);
  return;
}

// free hashing table without deleting elements inside
void free_ht (struct hashing_table *tab) {
  // for each bucket, free whathever's inside
  for (int i = 0; i < tab->tablelen; i++) {
    free_bucket (tab->table[i]);
  }
  // free the table
  free (tab->table);
  // and the structure
  free (tab);
  return;
}

// free hashing table and all elements inside
void free_ht_full (struct hashing_table *tab) {
  // for each bucket, free whathever's inside
  for (int i = 0; i < tab->tablelen; i++) {
    free_bucket_full (tab->table[i]);
  }
  // free the table
  free (tab->table);
  // and the structure
  free (tab);
  return;
}

uint64_t insert_hash_buf = 0;
// insert element into hashing table (if key is empty, use data instead)
int insert_elem_ht (struct hashing_table *tab, void *data, size_t datalen,
                    void *key, size_t keylen) {
  // compute hash of key or data depending on key usage
  if (keylen != 0) {
    insert_hash_buf = hash339 (tab->k, key, keylen);
  } else {
    insert_hash_buf = hash339 (tab->k, data, datalen);
  }
  // make the result fall withing our bucket size
  insert_hash_buf = insert_hash_buf % tab->tablelen;
  // if the bucket need first allocation
  if(tab->table[insert_hash_buf]->len==0 && tab->table[insert_hash_buf]->count==0) {
    // allocate an array of the default base bucket len
    tab->table[insert_hash_buf]->elems = malloc (sizeof (void *) * tab->base_bucketlen);
    // for each elements in default bucket len
    for (int j = 0; j < tab->base_bucketlen; j++) {
      // allocate a default number of bucket elements
      tab->table[insert_hash_buf]->elems[j] = malloc (sizeof (struct elem));
      // allocation error management
      if (tab->table[insert_hash_buf]->elems[j] == NULL) {
        perror ("malloc");
        exit (1);
      }
    }
    tab->table[insert_hash_buf]->len = tab->base_bucketlen;
  }
  // insert at corresponding bucket
  append_to_bucket (tab->table[insert_hash_buf], data, datalen, key, keylen);
  return 1;
}

uint64_t hash_retrieve_buf = 0;
// retrieve one element with given key
struct elem *retrieve_elem_ht (struct hashing_table *tab, void *key,
                               size_t keylen) {
  // compute hash
  hash_retrieve_buf = hash339(tab->k, key, keylen);
  // make the result fall withing our bucket size
  hash_retrieve_buf = hash_retrieve_buf % tab->tablelen;
  // we got a match for hash, now try to match each bucket element
  for (int j = 0; j < tab->table[hash_retrieve_buf]->count; j++) {
    // if key matches
    if(strcmp(key, tab->table[hash_retrieve_buf]->elems[j]->key)==0) {
      // we can return the elem adress
      return tab->table[hash_retrieve_buf]->elems[j];
    }
  }
  // if nothing was found return NULL
  return NULL;
}

uint64_t hash_delete_buf = 0;
// delete element with given key from hashing table (return number of deleted elements)
int delete_elem_ht (struct hashing_table *tab, void *key, size_t keylen) {
  // compute hash
  hash_delete_buf = hash339(tab->k, key, keylen);
  // make the result fall withing our bucket size
  hash_delete_buf = hash_delete_buf % tab->tablelen;
  // we got a match for hash, now try to match each bucket element
  for (int j = 0; j < tab->table[hash_delete_buf]->count; j++) {
    // if key matches
    if(strcmp(key, tab->table[hash_delete_buf]->elems[j]->key)==0) {
      // we need to delete the element in the bucket
      // we will first shift all elems above j down of one unit
      // effectively overridding the elem at j
      for (int k=j ; k<tab->table[hash_delete_buf]->count-1; k++) {
        tab->table[hash_delete_buf]->elems[k]->key = tab->table[hash_delete_buf]->elems[k+1]->key;
        tab->table[hash_delete_buf]->elems[k]->data = tab->table[hash_delete_buf]->elems[k+1]->data;
        tab->table[hash_delete_buf]->elems[k]->keylen = tab->table[hash_delete_buf]->elems[k+1]->keylen;
        tab->table[hash_delete_buf]->elems[k]->datalen = tab->table[hash_delete_buf]->elems[k+1]->datalen;
        // not sure if really usefull
        tab->table[hash_delete_buf]->elems[k]->hash_key = tab->table[hash_delete_buf]->elems[k+1]->hash_key;
      }
      // and the we decrement count of elems in bucket
      tab->table[hash_delete_buf]->count = tab->table[hash_delete_buf]->count - 1;
      // and we return number of elem deleted (1)
      return 1;
    }
  }
  // nothing was found, return the number of elem deleted (0)
  return 0;
}

// apend a key/data pair to the bucket, initializing the hash if there were no
// previous elements, realocate if no space left in bucket
void append_to_bucket (struct bucket *b, void *data, size_t datalen, void *key,
                       size_t keylen) {
  // if the bucket has no allocated free space, reject with error
  if (b->len == 0) {
    fprintf (stderr, "Send a bucket with no allocated space to "
                     "append_to_bucket !! aborting.\n");
    exit (1);
  }

  // if no space is left, we need to allocate another bloc
  if ((b->len) == b->count) {
    b->elems = realloc (b->elems,(b->len + BUCKET_ELEMS_BLOC_SIZE) * sizeof (void *));
    if (b->elems == NULL) {
      perror ("realloc");
      exit (1);
    }
    // for each new element
    for (int i = b->len; i < (b->len + BUCKET_ELEMS_BLOC_SIZE); i++) {
      b->elems[i] = malloc (sizeof (struct elem));
      if (b->elems[i] == NULL) {
        perror ("malloc");
        exit (1);
      }
    }
    // update new size of bucket
    b->len = b->len + BUCKET_ELEMS_BLOC_SIZE;
  }

  // copy pointers to the value in the first empty bucket
  b->elems[b->count]->key = key;
  b->elems[b->count]->data = data;
  b->elems[b->count]->keylen = keylen;
  b->elems[b->count]->datalen = datalen;
  b->elems[b->count]->hash_key = hash339 (DEFAULT_HASH_K, key, keylen);

  // increase bucket count
  b->count = b->count + 1;

  // shouldn't possibly happen (if so, we have a weird bug to fix)
  if (b->count > b->len) {
    fprintf (stderr,
             "Bucket count is above allocated len, programming bug...\n");
    exit (1);
  }

  return;
}


// function to test collusion on
uint32_t test_fun (uint64_t x) {
  uint32_t hi = x >> 32;
  uint32_t lo = x & 0xFFFFFFFF;

  hi += lo;
  lo = (lo << 5) ^ (lo >> 27);
  lo ^= hi;
  lo = (lo << 11) ^ (lo >> 21);

  return (lo + hi);
}

// second function to test collusion on
uint32_t test_fun2 (uint64_t x) { return 0; }
