#include "hash_table.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <string.h>

#define TEST_DATA_LENGTH 200

int main (int argc, char *argv[]) {

  printf ("Lancement du programme de test pour hash_table\n");

  struct hashing_table * ht = setup_ht(100, 4);

  // reading file with test data
  FILE *test_csv = fopen ("res/key_values.csv", "r");

  // test for errors
  if (test_csv == NULL) {
    perror ("fopen");
    exit (1);
  }

  // buffer to hold temporary values
  char buf1[50];

  // read the csv header
  fscanf (test_csv, "%s\n", buf1);

  // load some values to add to the hash table
  char ** keys = malloc(sizeof(void*) * TEST_DATA_LENGTH);
  char ** values = malloc(sizeof(void*) * TEST_DATA_LENGTH);
  // error management for malloc
  if(keys==NULL || values==NULL) {
    // will not necesseraly display right error if keys fail but values succeed
    // but I don't really care
    perror("malloc");
    return 1;
  }

  // read and allocate the values
  for(int i=0;i<TEST_DATA_LENGTH;i++) {
    keys[i] = malloc(sizeof(char) * 9);
    values[i] = malloc(sizeof(char) * 26);
    if(keys[i]==NULL || values[i]==NULL) {
      // will not necesseraly display right error if keys fail but values succeed
      // but I don't really care
      perror("malloc");
      return 1;
    }
    // read the csv line
    fscanf (test_csv, "%s\n", buf1);
    strcpy(keys[i],strtok(buf1, ","));
    strcpy(values[i], strtok(NULL, ","));
  }

  // push the values in the hash table
  for(int i=0;i<TEST_DATA_LENGTH;i++) {
    insert_elem_ht( ht , values[i], 26, keys[i], 9);
  }

  printf("Elements have been inserted into hash table.\n");

  // search for value with id 25
  struct elem * mon_elem = retrieve_elem_ht (ht, keys[25], 9);
  // technically we could directly compare pointer to see if they are the same elements
  // but for compatibility reasons with enventual other implementations in the future we'll strcmp
  if(strcmp(mon_elem->key, keys[25])!=0) {
    printf("Unable to find key 25.\n");
    printf("Failed to pass tests.\n");
    exit (1);
  }
  // search for value with id 125
  mon_elem = retrieve_elem_ht (ht, keys[125], 9);
  // technically we could directly compare pointer to see if they are the same elements
  // but for compatibility reasons with enventual other implementations in the future we'll strcmp
  if(strcmp(mon_elem->key, keys[125])!=0) {
    printf("Unable to find key 125.\n");
    printf("Failed to pass tests.\n");
    exit (1);
  }

  // delete value with id 125
  int nbdel = delete_elem_ht (ht, keys[125], 9);
  // test if delete succeeded
  if(nbdel!=1) {
    printf("Unable to delete key 125.\n");
    printf("Failed to pass tests.\n");
    exit (1);
  }

  // search for value with id 125
  mon_elem = retrieve_elem_ht (ht, keys[125], 9);
  // technically we could directly compare pointer to see if they are the same elements
  // but for compatibility reasons with enventual other implementations in the future we'll strcmp
  if(mon_elem!=NULL) {
    printf("Able to find key 125 after deletion.\n");
    printf("Failed to pass tests.\n");
    exit (1);
  }

  // delete value with id 125
  nbdel = delete_elem_ht (ht, keys[125], 9);
  // test if delete succeeded
  if(nbdel!=0) {
    printf("Able to double delete key 125.\n");
    printf("Failed to pass tests.\n");
    exit (1);
  }

  // free variables
  free_ht (ht);
  
  // read and allocate the values
  for(int i=0;i<TEST_DATA_LENGTH;i++) {
    free(keys[i]);
    free(values[i]);
  }

  free(keys);
  free(values);

  fclose(test_csv);
  
  printf("Tests passés avec succès.\n");

  return 0;
}
