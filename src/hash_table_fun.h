#ifndef DEF_HASH_TABLE
#define DEF_HASH_TABLE

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#define DEFAULT_HASH_K 309410
// the size of the blocs for realloc of elems into buckets
#define BUCKET_ELEMS_BLOC_SIZE 65536
#define HT_BUCKET_BLOC_SIZE 262144

struct elem {
  // key describing data
  void *key;
  // length of key
  size_t keylen;
  // data
  void *data;
  // length of data
  size_t datalen;
  // hash of key
  uint32_t hash_key;
};

struct bucket {
  // reference to elements
  struct elem **elems;
  // bucket size
  size_t len;
  // number of elements inside
  size_t count;
};

struct hashing_table {
  // reference to buckets
  struct bucket **table;
  // length of the table
  size_t tablelen;
  // default bucket size at creation
  size_t base_bucketlen;
  // hashing key
  uint64_t k;
};

// initialize hashing table
struct hashing_table *
setup_ht (size_t tablelen, size_t base_bucketlen);

// free memory without freeing elements inside
void free_bucket (struct bucket *b);

// free memory and all elements
void free_bucket_full (struct bucket *b);

// free hashing table without deleting elements inside
void free_ht (struct hashing_table *tab);

// free hashing table and all elements inside
void free_ht_full (struct hashing_table *tab);

// insert element into hashing table (if key is empty, use data instead)
// return the number of elements inserted
int insert_elem_ht (struct hashing_table *tab, void *data, size_t datalen,
                    void *key, size_t keylen);

// apend a key/data pair to the bucket, initializing the hash if there were no
// previous elements
void append_to_bucket (struct bucket *b, void *data, size_t datalen, void *key,
                       size_t keylen);

// retrieve one element with given key
struct elem *retrieve_elem_ht (struct hashing_table *tab, void *key,
                               size_t keylen);

// delete element with given key from hashing table
int delete_elem_ht (struct hashing_table *tab, void *key, size_t keylen);

// function to test collusion on
uint32_t test_fun (uint64_t x);

// second function to test collusion on
uint32_t test_fun2 (uint64_t x);

#endif // DEF_HASH_TABLE