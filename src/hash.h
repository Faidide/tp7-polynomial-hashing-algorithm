#ifndef DEF_SORT_H
#define DEF_SORT_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#define MAX_MOD 8589934583uLL

/* Run a multiplication module 2^33 - 9 */
uint64_t mul339 (uint64_t a, uint64_t b);

/* compute modulo */
uint64_t mod339 (uint64_t target);

/* Hash the integer k using polynomial hash modulo 2^33 - 9 */
uint64_t hash339 (uint64_t k, void *buf, size_t buflen);

#endif // DEF_HASH_H