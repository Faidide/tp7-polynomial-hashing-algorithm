import pandas as pd
import numpy as np
import random

DATASET_SIZE = 200

maximum_nb = (2**33 - 9)

term1 = [random.randint(0,maximum_nb) for j in range(DATASET_SIZE)]
term2 = [random.randint(0,maximum_nb) for j in range(DATASET_SIZE)]

data_dict = {
    "term1": term1,
    "term2": term2,
    "result": [str((term1[i]*term2[i])%maximum_nb) for i in range(DATASET_SIZE)]
}

dataset_mul = pd.DataFrame(data=data_dict)
dataset_mul.to_csv("../res/mult_mod.csv", index=False)