import pandas as pd
import numpy as np
import random
import string

DATASET_SIZE = 200

def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

data_dict = {
    "key": [get_random_string(8) for i in range(DATASET_SIZE)],
    "data": [get_random_string(25) for i in range(DATASET_SIZE)]
}

dataset = pd.DataFrame(data=data_dict)
dataset.to_csv("../res/key_values.csv", index=False)