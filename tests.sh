#!/bin/sh

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35m test_hash \033[33m ===========\033[0m"

printf "%-30.30s" "Execution des tests..."
./build/bin/test_hash > test/test_hash.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test/test_hash.output test/test_hash.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35m test_divide_mod \033[33m ===========\033[0m"

printf "%-30.30s" "Execution des tests..."
./build/bin/test_divide_mod > test/test_divide_mod.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test/test_divide_mod.output test/test_divide_mod.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35m test_hash_table \033[33m ===========\033[0m"

printf "%-30.30s" "Execution des tests..."
./build/bin/test_hash_table > test/test_hash_table.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test/test_hash_table.output test/test_hash_table.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo " "
echo -e "\033[32mTous les tests on été passés avec succès\033[0m"
