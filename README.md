# TP7 Tables de hachage
## Compilation
Pour compiler, il faut faire:
```bash
cd build
make
```

Il faudra éventuellement changer clang en gcc comme compilateur dans le makefile si nécessaire et ajouter cas échéant le flag -std=gnu11.

## Éxécution
Pour éxécuter, les binaires sont dans le dossier *build/bin/* et il faut les lancer quand un fichier externe est utilisé depuis le dossier racine:
```bash
./build/bin/search_fun_collision
```

## Rapport
Le rapport se trouve en pdf dans le dossier doc, il s'apelle rapport.pdf.